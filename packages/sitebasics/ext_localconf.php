<?php

defined('TYPO3_MODE') or die('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '
    @import "DIR:EXT:sitebasics/Configuration/TSconfig/Page" extension="tsconfig"
    '
);
