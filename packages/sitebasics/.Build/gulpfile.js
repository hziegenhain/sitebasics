const gulpex = require("@arminlinzbauer/gulpex");
gulpex(exports, {
    projectRoot: '.',
    documentRoot: '../',
    assetDirectory: '../assets', // not uses currently
    nodeModules: './node_modules',
    cssDirectory: '../Resources/Public/Styles',
    scriptsDir: '../Resources/Public/JavaScript',

    bundles: {
        myGlobalStyleBundle: {
            type: 'style',
            files: ["../Resources/Private/Sass/**/*.s[ca]ss"],
            sourcemaps: true
        },
        myJsLibs: {
            type: 'script',
            name: 'libraries.bundle.js',
            minify: false,
            sourcemaps: false,
            files: [
                './node_modules/jquery/dist/jquery.min.js',
                //'./node_modules/popper.js/dist/umd/popper.min.js',
                './node_modules/bootstrap/dist/js/bootstrap.min.js',
                './node_modules/slick-carousel/slick/slick.min.js',
            ],
        },
        myCustomJsScripts: {
            type: 'script',
            name: 'scripts.bundle.js',
            minify: true,
            files: ['../Resources/Private/JavaScript/**/*.js'],
        }
    },
});
