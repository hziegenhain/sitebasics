<?php

defined('TYPO3_MODE') or die('Access denied.');

call_user_func(
    function () {
        /**
         * Default TypoScript
         */
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            'sitebasics',
            'Configuration/TypoScript',
            'Basic site templates'
        );
    }
);
