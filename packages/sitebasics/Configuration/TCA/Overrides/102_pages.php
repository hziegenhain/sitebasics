<?php

defined('TYPO3_MODE') or die('Access denied.');

$GLOBALS['TCA']['pages']['columns']['slug']['config']['generatorOptions']['fields'] = [['nav_title', 'title']];
$GLOBALS['TCA']['pages']['columns']['slug']['config']['generatorOptions']['replacements'] = ['/' => '-'];
