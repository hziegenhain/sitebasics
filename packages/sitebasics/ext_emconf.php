<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Sitebasics',
    'description' => 'Classes for the TYPO3 backend.',
    'category' => 'be',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Henrik Ziegenhain',
    'author_email' => 'henrik@hziegenhain.de',
    'author_company' => 'HZiegenhain',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0',
        ],
    ],
];
