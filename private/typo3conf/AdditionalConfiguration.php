<?php

$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
    $GLOBALS['TYPO3_CONF_VARS'],
    [
        'DB' => [
            'Connections' => [
                'Default' => [
                    'dbname' => '###DB_NAME###',
                    'host' => '###DB_HOST###',
                    'password' => '###DB_PASSWORD###',
                    'port' => '3306',
                    'user' => '###DB_USER###',
                ],
            ],
        ],
    ]
);

switch (\TYPO3\CMS\Core\Core\Environment::getContext()) {
    case 'Production/Staging':
        $GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
            $GLOBALS['TYPO3_CONF_VARS'],
            [
                'SYS' => [
                    'displayErrors' => '-1',
                    'devIPmask' => '',
                    'errorHandler' => 'TYPO3\\CMS\\Core\\Error\\ErrorHandler',
                    'belogErrorReporting' => '0',
                ],
                'GFX' => [
                    'processor_path' => '/usr/bin/',
                    'processor_path_lzw' => '/usr/bin/',
                ]
            ]
        );
        break;
    case 'Development/Local':
        if (getenv('IS_DDEV_PROJECT') === 'true') {
            $GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
                $GLOBALS['TYPO3_CONF_VARS'],
                [
                    'DB' => [
                        'Connections' => [
                            'Default' => [
                                'dbname' => 'db',
                                'host' => 'db',
                                'password' => 'db',
                                'port' => '3306',
                                'user' => 'db',
                            ],
                        ],
                    ],
                    // This GFX configuration allows processing by installed ImageMagick 6
                    'GFX' => [
                        'processor' => 'ImageMagick',
                        'processor_path' => '/usr/bin/',
                        'processor_path_lzw' => '/usr/bin/',
                    ],
                    // This mail configuration sends all emails to mailhog
                    'MAIL' => [
                        'transport' => 'smtp',
                        'transport_smtp_server' => 'localhost:1025',
                    ],
                    'SYS' => [
                        'trustedHostsPattern' => '.*.*',
                        'devIPmask' => '*',
                        'displayErrors' => 1,
                    ],
                ]
            );
        }
        break;
    default:
}
